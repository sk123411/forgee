package com.forgealumnus.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.forgealumnus.R;
import com.forgealumnus.model.AdvisorPojo;

import java.util.ArrayList;


public class AdvisorAdapter extends RecyclerView.Adapter<AdvisorAdapter.ViewHolder> {


    private ArrayList<AdvisorPojo> cityPogos;
    Context context;
    String ManualCity="",CityPageStatus="";



    public AdvisorAdapter(ArrayList<AdvisorPojo> cityPogos, Context context) {

        super();
        this.cityPogos=cityPogos;
        this.context = context;

    }




    @Override
    public AdvisorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.advisor_list, parent, false);

        AdvisorAdapter.ViewHolder viewHolder = new AdvisorAdapter.ViewHolder(view);

        return viewHolder;

    }


    @Override
    public void onBindViewHolder(AdvisorAdapter.ViewHolder Viewholder, int position) {


        final AdvisorPojo dataAdapterOBJ = cityPogos.get(position);

        Viewholder.txt_name.setText(dataAdapterOBJ.getName());



    }

    @Override
    public int getItemCount() {

        return cityPogos.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_advisor;
        public TextView txt_name,txt_desc;



        public ViewHolder(View itemView) {

            super(itemView);
            iv_advisor=itemView.findViewById(R.id.iv_advisor);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_desc=itemView.findViewById(R.id.txt_desc);

            }
            }



}


