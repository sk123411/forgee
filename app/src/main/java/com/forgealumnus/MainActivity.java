package com.forgealumnus;

import androidx.annotation.RequiresApi;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.forgealumnus.activity.SElectUserActiv;
import com.forgealumnus.others.AppConstats;
import com.forgealumnus.others.SharedHelper;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String Id=SharedHelper.getKey(getApplicationContext(), AppConstats.USER_ID);

        new Handler().postDelayed(new Runnable() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void run() {
                if (Id.equals("")){
                    startActivity(new Intent(MainActivity.this, SElectUserActiv.class));
                    finish();
                }else {
                    startActivity(new Intent(MainActivity.this, DrawerHomeActivity.class));
                    finish();
                }


            }
        }, 3000);
    }
}