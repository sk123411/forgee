package com.forgealumnus.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.forgealumnus.R;
import com.forgealumnus.others.AppConstats;
import com.forgealumnus.others.SharedHelper;

public class SElectUserActiv extends AppCompatActivity {

TextView txt_next;
RadioButton rd_student,rd_alumini;
String UserType="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_elect_user);

        txt_next=findViewById(R.id.txt_next);
        rd_student=findViewById(R.id.rd_student);
        rd_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd_alumini.setChecked(false);

                UserType="1";
            }
        });
        rd_alumini=findViewById(R.id.rd_alumini);
        rd_alumini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd_student.setChecked(false);
                UserType="2";
            }
        });
        txt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txt_next.setBackgroundResource(R.drawable.red_rounded);
                txt_next.setTextColor(getResources().getColor(R.color.black));



                if (UserType.equals("")){
                    Toast.makeText(SElectUserActiv.this, "Please select user type", Toast.LENGTH_SHORT).show();
                }else {
                    SharedHelper.putKey(getApplicationContext(), AppConstats.USER_TYPE, UserType);
                    startActivity(new Intent(SElectUserActiv.this, SignUpActiv.class));
                }

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        txt_next.setBackgroundResource(R.drawable.black_rounded);
        txt_next.setTextColor(getResources().getColor(R.color.gold));



    }
}