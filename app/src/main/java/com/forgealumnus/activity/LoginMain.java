package com.forgealumnus.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.forgealumnus.DrawerHomeActivity;
import com.forgealumnus.HomeActivity;
import com.forgealumnus.R;
import com.forgealumnus.helper.Constant;
import com.forgealumnus.model.SignUpResponse;
import com.forgealumnus.others.API;

import io.paperdb.Paper;

public class LoginMain extends AppCompatActivity {
    TextView txt_login,txt_signup;
    EditText edt_email,edt_pass;
    CardView card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);


        txt_login=findViewById(R.id.txt_login);
        txt_signup=findViewById(R.id.txt_signup);
        edt_email=findViewById(R.id.edt_email);
        edt_pass=findViewById(R.id.edt_pass);
        card=findViewById(R.id.card);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_login.setBackgroundResource(R.drawable.red_rounded);
                txt_login.setTextColor(getResources().getColor(R.color.black));




                String stremail=edt_email.getText().toString().trim();
                String strpass=edt_pass.getText().toString().trim();



                if (TextUtils.isEmpty(stremail)) {
                    Toast.makeText(LoginMain.this, "Please enter email", Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(strpass)) {
                    Toast.makeText(LoginMain.this, "Please enter password", Toast.LENGTH_SHORT).show();

                } else {

                    login(stremail,strpass);
                }


            }
        });
        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginMain.this, SignUpActiv.class));
            }
        });

    }


      private void login(String stremail, String strpass) {
          Log.e("aefafaf", stremail+"email" );
          Log.e("aefafaf", strpass+"password" );
        card.setVisibility(View.VISIBLE);
        AndroidNetworking.post(API.BASE_URL)
                .addBodyParameter("control", "login")
                .addBodyParameter("email", stremail)
                .addBodyParameter("password", strpass)
                .setTag("signUpOrLogin")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(SignUpResponse.class, new ParsedRequestListener<SignUpResponse>() {
                    @Override
                    public void onResponse(SignUpResponse response) {


                        if (response.getResult()){

                            Paper.init(getApplicationContext());
                            Paper.book().write(Constant.USER_DETAILS,response);

                            startActivity(new Intent(LoginMain.this, DrawerHomeActivity.class));

                        }else {


                        }



                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }


    @Override
    protected void onResume() {
        super.onResume();
        txt_login.setBackgroundResource(R.drawable.black_rounded);
        txt_login.setTextColor(getResources().getColor(R.color.gold));

    }
}
