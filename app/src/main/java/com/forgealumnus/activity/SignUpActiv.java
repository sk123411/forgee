package com.forgealumnus.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.forgealumnus.R;
import com.forgealumnus.model.SignUpResponse;
import com.forgealumnus.others.API;
import com.forgealumnus.others.AppConstats;
import com.forgealumnus.others.SharedHelper;

public class SignUpActiv extends AppCompatActivity {

TextView txt_signup,txt_login;
EditText edt_firstname,edt_lastname,edt_email,edt_num,edt_pass;
String User_type="";
CardView      card;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        User_type= SharedHelper.getKey(SignUpActiv.this, AppConstats.USER_TYPE);
        txt_login=findViewById(R.id.txt_login);
        txt_signup=findViewById(R.id.txt_signup);
        edt_firstname=findViewById(R.id.edt_firstname);
        edt_lastname=findViewById(R.id.edt_lastname);
        edt_email=findViewById(R.id.edt_email);
        edt_num=findViewById(R.id.edt_num);
        edt_pass=findViewById(R.id.edt_pass);
        card=findViewById(R.id.card);
        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_signup.setBackgroundResource(R.drawable.red_rounded);
                txt_signup.setTextColor(getResources().getColor(R.color.black));



                String strfirstname=edt_firstname.getText().toString().trim();
               String strlastname=edt_lastname.getText().toString().trim();
               String stremail=edt_email.getText().toString().trim();
               String strnumber=edt_num.getText().toString().trim();
               String strpass=edt_pass.getText().toString().trim();

                if (TextUtils.isEmpty(strfirstname)) {
                    Toast.makeText(SignUpActiv.this, "Please enter first name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(strlastname)) {
                    Toast.makeText(SignUpActiv.this, "Please enter last name", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(stremail)) {
                    Toast.makeText(SignUpActiv.this, "Please enter email", Toast.LENGTH_SHORT).show();

                }  else if (TextUtils.isEmpty(strnumber) && strnumber.length()<10) {
                    Toast.makeText(SignUpActiv.this, "Please enter number and of 10 digits", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(strpass)) {
                    Toast.makeText(SignUpActiv.this, "Please enter password", Toast.LENGTH_SHORT).show();

                } else {

                    Signup(strfirstname,strlastname,stremail,strnumber,strpass);
                }



            }
        });

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActiv.this, LoginMain.class));
            }
        });
    }

    private void Signup(String strfirstname, String strlastname, String stremail, String strnumber, String strpass) {
        Log.e("advadddds", strfirstname+"firstname");
        Log.e("advadddds", strlastname+"lastname");
        Log.e("advadddds", stremail+"email");
        Log.e("advadddds", strnumber+"phone");
        Log.e("advadddds", strpass+"password");
        card.setVisibility(View.VISIBLE);
        AndroidNetworking.post(API.BASE_URL)
                .addBodyParameter("control", "signup")
                .addBodyParameter("first_name", strfirstname)
                .addBodyParameter("last_name", strlastname)
                .addBodyParameter("phone", strnumber)
                .addBodyParameter("password", strpass)
                .addBodyParameter("email", stremail)
                .addBodyParameter("type", User_type)
                .setTag("signUpOrLogin")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(SignUpResponse.class, new ParsedRequestListener<SignUpResponse>() {
                    @Override
                    public void onResponse(SignUpResponse response) {
                        if (response.getResult()) {




                            startActivity(new Intent(getApplicationContext(),LoginMain.class));

                        } else {
                                String message = response.getMessage();
                                Toast.makeText(SignUpActiv.this, message, Toast.LENGTH_SHORT).show();

                            }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });






//
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        card.setVisibility(View.GONE);
//                        if (response != null) {
//
//                            try {
//
//
//                            } catch (Exception e) {
//                                Log.e("exceptionTag", "onResponse: " + e.getMessage());
//                                card.setVisibility(View.GONE);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        card.setVisibility(View.GONE);
//
//                    }
//                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        txt_signup.setBackgroundResource(R.drawable.black_rounded);
        txt_signup.setTextColor(getResources().getColor(R.color.gold));

    }
}