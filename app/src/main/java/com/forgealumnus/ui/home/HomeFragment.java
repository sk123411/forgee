package com.forgealumnus.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.forgealumnus.MemberActiv;
import com.forgealumnus.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    TextView txt_membership;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_membership, container, false);
        txt_membership=root.findViewById(R.id.txt_membership);
        txt_membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MemberActiv.class));
            }
        });


        return root;
    }
}