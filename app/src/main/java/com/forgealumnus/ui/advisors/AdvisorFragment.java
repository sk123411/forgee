package com.forgealumnus.ui.advisors;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.forgealumnus.CandidateApplicationActivity;
import com.forgealumnus.R;
import com.forgealumnus.adapter.AdvisorAdapter;
import com.forgealumnus.model.AdvisorPojo;
import com.forgealumnus.others.API;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdvisorFragment extends Fragment {

    private AdvisorViewModel advisorViewModel;
    TextView txtNect;
    RecyclerView rv_advisor;
    ArrayList<AdvisorPojo>advisorPojos;
    AdvisorAdapter advisorAdapter;
    CardView card;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        advisorViewModel =
                new ViewModelProvider(this).get(AdvisorViewModel.class);
        View root = inflater.inflate(R.layout.fragment_advisors, container, false);

        txtNect=root.findViewById(R.id.txtNect);
        rv_advisor=root.findViewById(R.id.rv_advisor);
        card=root.findViewById(R.id.card);
        txtNect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              startActivity(new Intent(getActivity(), CandidateApplicationActivity.class));
            }
        });
        ShowAdvisor();

        return root;
    }

    private void ShowAdvisor() {
        card.setVisibility(View.VISIBLE);
        AndroidNetworking.post(API.BASE_URL)
                .addBodyParameter("control", "advisor_list1")
                .setTag("signUpOrLogin")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        card.setVisibility(View.GONE);
                        advisorPojos = new ArrayList<>();
                        if (response != null) {

                            try {

                                if (response.getString("result").equals("true")) {

                                    String data = response.getString("data");
                                    JSONArray jsonArray = new JSONArray(data);
                                    AdvisorPojo advisorPojo=new AdvisorPojo();
                                    for (int i = 0; i <jsonArray.length() ; i++) {
                                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                        advisorPojo.setId(jsonObject1.getString("id"));
                                        advisorPojo.setName(jsonObject1.getString("first_name")+" ,"+jsonObject1.getString("last_name"));
                                        advisorPojos.add(advisorPojo);

                                    }
                                    rv_advisor.setHasFixedSize(true);
                                    rv_advisor.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
                                    rv_advisor.setAdapter(new AdvisorAdapter(advisorPojos,getActivity()));

                                } else {
                                    String message = response.getString("message");
                                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                Log.e("exceptionTag", "onResponse: " + e.getMessage());
                                card.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        card.setVisibility(View.GONE);

                    }
                });

    }

}
